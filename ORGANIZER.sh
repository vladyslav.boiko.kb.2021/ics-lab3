# This is the script content
#!/bin/bash

# Перевіряємо, чи був переданий шлях до каталогу як аргумент
if [ -z "$1" ]; then
  echo "Будь ласка, вкажіть шлях до каталогу."
  exit 1
fi

# Задаємо змінну для каталогу
DIR_ORGANIZE="$1"

# Створюємо підкаталоги, якщо вони не існують
mkdir -p "$DIR_ORGANIZE/documents"
mkdir -p "$DIR_ORGANIZE/images"
mkdir -p "$DIR_ORGANIZE/other"

# Лічильники файлів
doc_count=0
img_count=0
other_count=0

# Переміщуємо файли до відповідних підкаталогів
for file in "$DIR_ORGANIZE"/*; do
  if [ -f "$file" ]; then
    extension="${file##*.}"
    case "$extension" in
      doc|pdf|txt)
        mv "$file" "$DIR_ORGANIZE/documents/"
        ((doc_count++))
        ;;
      jpg|png|gif)
        mv "$file" "$DIR_ORGANIZE/images/"
        ((img_count++))
        ;;
      *)
        mv "$file" "$DIR_ORGANIZE/other/"
        ((other_count++))
        ;;
    esac
  fi
done

# Друкуємо кількість переміщених файлів
echo "Переміщено $doc_count файлів у $DIR_ORGANIZE/documents"
echo "Переміщено $img_count файлів у $DIR_ORGANIZE/images"
echo "Переміщено $other_count файлів у $DIR_ORGANIZE/other"